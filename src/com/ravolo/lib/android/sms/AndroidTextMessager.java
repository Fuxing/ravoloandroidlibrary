package com.ravolo.lib.android.sms;

import android.telephony.SmsManager;

/**
 * Simple SMS Sending service
 * @author Fuxing
 *
 */
public class AndroidTextMessager {
	private SmsManager sms;

	/**
	 * Construct SMS sending service
	 */
	public AndroidTextMessager() {
		sms = SmsManager.getDefault();
	}

	/**
	 * 
	 * @param phoneNumber
	 * @param message
	 */
	public void sendSMS(String phoneNumber, String message) {
		sms.sendTextMessage(phoneNumber, null, message, null, null);
	}
	
	/**
	 * 
	 * @param phoneNumber
	 * @param message
	 */
	public static void send(String phoneNumber, String message) {
		SmsManager.getDefault().sendTextMessage(phoneNumber, null, message, null, null);
	}

}
