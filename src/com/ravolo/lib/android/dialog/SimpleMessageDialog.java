package com.ravolo.lib.android.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class SimpleMessageDialog {
	private Context context;
	private AlertDialog.Builder dialog;
	private AlertDialog alertDialog;

	public SimpleMessageDialog(Context context) {
		this.context = context;
	}

	private void buildDialog(String message) {
		dialog = new AlertDialog.Builder(context);
		dialog.setMessage(message);
		dialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int id) {
		    	dialog.dismiss();
		     }
		});
		alertDialog = dialog.create();
	}

	public void displayMessage(String message) {
		buildDialog(message);
		alertDialog.show();
	}
	
	public void showMessage(String message) {
		buildDialog(message);
		alertDialog.show();
	}
}
