package com.ravolo.lib.android.mail;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

/**
 * An Email sending service for android.
 * 
 * @author Fuxing
 * 
 */
public class SendEmailByIntent {
	/**
	 * Intent to start the mail
	 */
	private Intent intent;

	/**
	 * Constructor to build the mail sending service, single mail
	 */
	public SendEmailByIntent(String recipientEmailAdress, String subject, String body) {
		buildMailIntent(new String[] { recipientEmailAdress }, subject, body);
	}

	/**
	 * Constructor to build the mail sending service, multiple mail
	 */
	public SendEmailByIntent(String[] recipientEmailAdresses, String subject, String body) {
		buildMailIntent(recipientEmailAdresses, subject, body);
	}

	/**
	 * This build the mail intent to consume
	 * 
	 * @param receiverEmailAdresses
	 * @param subject
	 * @param body
	 */
	private void buildMailIntent(String[] recipientEmailAdresses,
			String subject, String body) {
		intent = new Intent(Intent.ACTION_SEND);
		intent.setType("message/rfc822");
		intent.putExtra(Intent.EXTRA_EMAIL, recipientEmailAdresses);
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_TEXT, body);
	}

	/**
	 * Start the mail sending progress.
	 * 
	 * @param activity
	 * @return
	 */
	public boolean start(Activity activity) {
		try {
			activity.startActivity(Intent.createChooser(intent, "Send mail..."));
			return true;
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(activity, "There are no email clients installed.",
					Toast.LENGTH_SHORT).show();
			return false;
		}
	}
}
