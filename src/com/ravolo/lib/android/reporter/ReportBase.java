package com.ravolo.lib.android.reporter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.ravolo.lib.android.manager.NetworkConnection;

/**
 * Report module
 * 
 * @author Fuxing
 * 
 */
public class ReportBase {
	public static final String LOG_TAG = "REPORT DIAGNOSTIC";

	/**
	 * Reported Logged Data
	 * 
	 * @author Fuxing
	 * 
	 */
	class Data {
		protected String id;
		protected String user;
		protected String message;
		protected String exception;
	}

	private Data data;
	private Context context;

	/**
	 * internal constructor
	 * 
	 * @param context
	 */
	ReportBase(Context context) {
		this.context = context;
	}

	/**
	 * internal
	 * 
	 * @param data
	 */
	void setData(Data data) {
		this.data = data;
	}

	/**
	 * Send exception data to the server.
	 * 
	 * @param context
	 * @param exception
	 * @return
	 */
	public static ReportBase exception(Context context, Exception exception) {
		return exception(context, exception, null);
	}

	/**
	 * Send exception data to the server.
	 * 
	 * @param context
	 * @param exception
	 * @param message
	 * @return
	 */
	public static ReportBase exception(Context context, Exception exception,
			String message) {
		ReportBase report = new ReportBase(context);
		Data data = report.new Data();
		data.message = message;
		if (exception != null) {
			data.exception = exception.getMessage();
		}
		data.user = report.getUser();
		report.setData(data);
		return report;
	}

	/**
	 * Send a message to server
	 * 
	 * @param context
	 * @param message
	 * @return
	 */
	public static ReportBase exception(Context context, String message) {
		return message(context, message);
	}

	/**
	 * Send a message to server
	 * 
	 * @param context
	 * @param message
	 * @return
	 */
	public static ReportBase message(Context context, String message) {
		ReportBase report = new ReportBase(context);
		Data data = report.new Data();
		data.message = message;
		data.user = report.getUser();
		report.setData(data);
		return report;
	}

	/**
	 * Send report data
	 */
	public void send() {
		// Local log
		String localMessage = "";
		if (data.user != null) {
			localMessage += "User: " + data.user + "\n";
		}
		if (data.message != null) {
			localMessage += "Message: " + data.message + "\n";
		}
		if (data.exception != null) {
			localMessage += "Exception Message: " + data.exception;
		}
		Log.w(LOG_TAG, localMessage);
		// Online send
		// Check if they allow
		if (!isAllowedToSendDiagnosticData()) {
			// Not allowed and end this
			return;
		}
		NetworkConnection network = new NetworkConnection(context);
		if (network.isConnected()) {
			// Send it
			sendToServer(data);
		} else {
			Log.w(LOG_TAG, "No network connection to send diagnostic data.");
		}
	}

	protected void sendToServer(Data data) {

	}

	/**
	 * 
	 * @return
	 */
	public boolean isAllowedToSendDiagnosticData() {
		SharedPreferences settings = getSharedPreferences("DiagnosticSetting");
		return settings.getBoolean("Allow", true);
	}

	/**
	 * Set if to allow application to send diagnostic data
	 * 
	 * @param allow
	 */
	public void setAllowedToSendDiagnosticData(boolean allow) {
		SharedPreferences settings = getSharedPreferences("DiagnosticSetting");
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean("Allow", allow);
		editor.commit();
	}

	/**
	 * Get user info
	 * 
	 * @return
	 */
	public String getUser() {
		SharedPreferences settings = getSharedPreferences("USER_MANAGER");
		String userInfo = "";
		userInfo += settings.getString("USER_username", "No username");
		userInfo += settings.getString("USER_email", " and email data.");
		return userInfo;
	}

	/**
	 * 
	 * @return
	 */
	public SharedPreferences getSharedPreferences(String prefName) {
		SharedPreferences settings = context.getSharedPreferences(prefName, 0);
		return settings;

	}
}