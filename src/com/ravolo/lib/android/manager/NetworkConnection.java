package com.ravolo.lib.android.manager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.ravolo.android.R;

public class NetworkConnection {
	private ConnectivityManager conMgr;
	private Activity activity;
	private Context context;

	private boolean connected;

	public NetworkConnection(Activity activity) {
		this.activity = activity;
		context = activity;
		init();

	}

	private void init() {

		conMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
		connected = activeNetwork != null
				&& activeNetwork.isConnectedOrConnecting();
	}

	public NetworkConnection(Context context) {
		this.context = context;
		init();
	}

	public boolean isConnected() {
		return connected;
		/*
		 * return conMgr.getNetworkInfo(0).getState() == State.CONNECTED ||
		 * conMgr.getNetworkInfo(1).getState() == State.CONNECTING;
		 */
	}

	public boolean isDisconnected() {
		return connected == false;
		/*
		 * return conMgr.getNetworkInfo(0).getState() == State.DISCONNECTED ||
		 * conMgr.getNetworkInfo(1).getState() == State.DISCONNECTED;
		 */
	}

	public void showNoNetworkTryAgain(
			final DialogInterface.OnClickListener onClickListener) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				AlertDialog noNetworkDialog = new AlertDialog.Builder(activity)
						.setPositiveButton(
								activity.getString(R.string.tryagain),
								onClickListener)
						.setMessage(R.string.no_network_available).create();
				noNetworkDialog.show();
			}
		});
	}
}
