package com.ravolo.lib.android.nav;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.ravolo.android.R;

/**
 * Refer to wiki document to understand how to use <br/>
 * Version: 0.4
 * @author Fuxing
 * 
 */
public abstract class NavDrawerFragmentActivity extends FragmentActivity {
	private String[] mActivityTitles;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;

	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;

	/**
	 * 
	 * @param layoutResourceId
	 */
	@Override
	public void setContentView(int layoutResourceId) {
		super.setContentView(R.layout.nav_drawer_main);
		setActivityContentView(layoutResourceId);
		initNavDrawer();
	}

	/**
	 * Needed to get the drawer titles
	 * 
	 * @return
	 */
	protected abstract String[] getNavDrawerListTitles();

	@SuppressLint("NewApi")
	private void initNavDrawer() {
		mActivityTitles = getNavDrawerListTitles();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		// Set the adapter for the list view
		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_list_item, mActivityTitles));
		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}
		};

		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			getActionBar().setHomeButtonEnabled(true);
		}
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView parent, View view, int position,
				long id) {
			selectedDrawerItem(position);
		}
	}

	/**
	 * When item select by index do what
	 * 
	 * @param position
	 */
	protected abstract void selectedDrawerItem(int position);

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * Check if the drawer is open, must add in API
	 * 
	 * @return
	 */
	protected boolean isDrawerOpen() {
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		return drawerOpen;
	}

	/**
	 * 
	 */
	protected void closeDrawer() {
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	/**
	 * 
	 */
	protected void openDrawer() {
		mDrawerLayout.openDrawer(mDrawerList);
	}

	/**
	 * 
	 * @param title
	 */
	protected void setActivityNameWhenDrawerOpened(String title) {
		mDrawerTitle = title;
	}

	/**
	 * 
	 * @param title
	 */
	protected void setActivityNameWhenDrawerClosed(String title) {
		mTitle = title;
	}

	/**
	 * 
	 * @param position
	 */
	public void highlightDrawerItem(int position) {
		mDrawerList.setItemChecked(position, true);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		if (mDrawerToggle != null) {
			mDrawerToggle.syncState();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (mDrawerToggle != null) {
			mDrawerToggle.onConfigurationChanged(newConfig);
		}
	}

	// need edit
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Pass the event to ActionBarDrawerToggle, if it returns
		// true, then it has handled the app icon touch event
		if (isDrawerToggled(item)) {
			return true;
		}
		// Handle your other action bar items...

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Check if they toggled the drawer
	 * 
	 * @param item
	 * @return
	 */
	public boolean isDrawerToggled(MenuItem item) {
		return mDrawerToggle.onOptionsItemSelected(item);
	}

	/**
	 * Add the view on top of the context view
	 * 
	 * @param layoutResourceId
	 */
	protected void setActivityContentView(int layoutResourceId) {
		FrameLayout contentBase = (FrameLayout) this
				.findViewById(R.id.nav_drawer_content_frame);
		// Remove existing view from content base.
		contentBase.removeAllViewsInLayout();
		// Inflater service
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(layoutResourceId, null);

		contentBase.addView(view);
	}
}
