package com.ravolo.lib.android.textview;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

public abstract class RightDrawableOnTouchListener implements OnTouchListener {
	Drawable drawable;
	private int fuzz = 10;

	/**
	 * @param keyword
	 */
	public RightDrawableOnTouchListener(TextView view) {
		super();
		final Drawable[] drawables = view.getCompoundDrawables();
		if (drawables != null && drawables.length == 2 + DOUBLE)
			this.drawable = drawables[DOUBLE];
	}

	public static final int DOUBLE = 2;
	public static final int SINGLE = 1;

	/**
	 * @param keyword
	 */
	public RightDrawableOnTouchListener(TextView view, int type) {
		super();
		final Drawable[] drawables = view.getCompoundDrawables();
		if (drawables != null && drawables.length == 2 + DOUBLE)
			this.drawable = drawables[DOUBLE];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnTouchListener#onTouch(android.view.View,
	 * android.view.MotionEvent)
	 */
	@Override
	public boolean onTouch(final View v, final MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN && drawable != null) {
			final int x = (int) event.getX();
			final int y = (int) event.getY();
			final Rect bounds = drawable.getBounds();
			/*
			 * final int right = v.getRight(); final int left = v.getLeft();
			 * final int top = v.getTop(); final int bottom = v.getBottom();
			 * final int paddingLeft = v.getPaddingLeft();
			 */
			final int paddingTop = v.getPaddingTop();
			final int paddingBottom = v.getPaddingBottom();
			final int paddingRight = v.getPaddingRight();
			final int height = v.getHeight();
			final int width = v.getWidth();

			if (x >= (width - bounds.width() - fuzz)
					&& x <= (width - paddingRight + fuzz)
					&& y >= (paddingTop - fuzz)
					&& y <= (height - paddingBottom) + fuzz) {
				return onDrawableTouch(event);
			}
		}
		return false;
	}

	public abstract boolean onDrawableTouch(final MotionEvent event);

}