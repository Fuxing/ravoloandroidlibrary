package com.ravolo.java.lib.debugger;

import java.util.ArrayList;

public class ListPrinter {
	public static String print(String[] strings) {
		String string = "Debugger Tool, Size: " + strings.length + "\n";

		for (String s : strings) {
			string += s + "\n";
		}

		return string;
	}

	public static String print(ArrayList<String> strings) {
		String string = "Debugger Tool, Size: " + strings.size() + "\n";

		for (String s : strings) {
			string += s + "\n";
		}

		return string;
	}
	
}
